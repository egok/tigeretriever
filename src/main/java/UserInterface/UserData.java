package UserInterface;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Copyright registered Hoffred AB
 *
 * This class is intended to get user data from CLI input.
 */
public class UserData
{

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTranPassword(String tranPassword) {
        this.tranPassword = tranPassword;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Login Credentials required by Sharekhan Sockets
     */
    private String username;
    private String password;
    private String tranPassword;
    private String ip;

    private Scanner myScanner;

    public void getUserDatafromCLI()
    {
        initilizeScanner();
        System.out.println("Enter the user name: ");
        this.username = myScanner.next();
        System.out.println("Enter the password: ");
        this.password = myScanner.next();
        System.out.println("Enter the trading password: ");
        this.tranPassword = myScanner.next();
        System.out.println("Enter the IP: ");
        this.ip = myScanner.next();

        myScanner.close();
    }

    private void initilizeScanner()
    {
        myScanner = new Scanner(System.in);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getTranPassword() {
        return tranPassword;
    }

    public String getIp() {
        return ip;
    }

    public UserData getUserDataFromSecretFile()
    {
        String secretFile = "secrets";
        String line = null;
        UserData userData = new UserData();
        try
        {
            FileReader fileReader = new FileReader(secretFile);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            line = bufferedReader.readLine();
            userData.setUsername(line);
            line = bufferedReader.readLine();
            userData.setPassword(line);
            line = bufferedReader.readLine();
            userData.setTranPassword(line);
            line = bufferedReader.readLine();
            userData.setIp(line);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return userData;
    }
}
