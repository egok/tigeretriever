package ShareKhanApi;

import ShareKhanApi.MarketDepth.MarketDepth;
import ShareKhanApi.ScripMaster.DataStructs.ScripMasterHashMap;
import ShareKhanApi.ScripMaster.ScripMasterCore;
import UserInterface.UserData;

import java.io.IOException;
import java.time.temporal.ValueRange;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class APIImpl implements API
{
    public ScripMasterHashMap scripMasterHashMap;


    private SharekhanAPI sharekhanAPI = null;
    private UserData userData;

    @Override
    public Set<Integer> getScrips() {
        return null;
    }

    @Override
    public Set<Integer> getScrips(ScripFilter value) {
        Set<Integer> scripIds = ScripMasterCore.getStocksBetweenLimitPrice(sharekhanAPI,
                value.getPriceRangeUpperLimit(),
                value.getPriceRangeLowerLimit(),
                scripMasterHashMap.getAllIds());

        return scripIds;
    }

    @Override
    public MarketDepth getMarket(int scripId) {
        return null;
    }

    @Override
    public HashMap<Integer, MarketDepth> getMarket(ArrayList<Integer> scripIds) {
        return null;
    }

/*    public void tempMethod()
    {
        userData = new UserData().getUserDataFromSecretFile();

        try {
            sharekhanAPI = new SharekhanAPI(userData.getUsername(), userData.getPassword(), userData.getTranPassword(), userData.getIp());
            sharekhanAPI.connect();
            Thread.sleep(2000L);
            sharekhanAPI.initilizeScripObjects();
//            ScripMasterHashMap scripMasterHashMap = ScripMasterCore.getOnlyEQSegment(sharekhanAPI.ncScripmasterMap);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Initilizes ShareKhanApi class object
     * Initilizes UserData class object from secret file
     * The calling thread will sleep for 2000 milli seconds
     */
    public void initilizeShareKhanAPI()
    {
        userData = new UserData().getUserDataFromSecretFile();
        try
        {
            sharekhanAPI = new SharekhanAPI(userData.getUsername(), userData.getPassword(), userData.getTranPassword(), userData.getIp());
            sharekhanAPI.connect();
            Thread.sleep(2000L);
            sharekhanAPI.sendScripMasterRequest();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Initilizes all the Scrip storing objects
     * After calling this method (after calling initilizeShareKhanAPI method), all the other methods in this class can be used.
     */
    public void initilizeScripObjects()
    {
        try
        {
            sharekhanAPI.sendScripMasterRequest();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        initilizeScripMasterHashMap();
    }

    private void initilizeScripMasterHashMap()
    {
        scripMasterHashMap = ScripMasterCore.getOnlyEQSegment(sharekhanAPI.ncScripmasterMap);
    }

}
