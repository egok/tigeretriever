package ShareKhanApi.ScripMaster;

import ShareKhanApi.MessageHeader;
import StaticData.CConstants;

import java.io.UnsupportedEncodingException;

public class ScriptMasterResponse
{
    public MessageHeader msgheader;
    public String exchangecode;

    public ScriptMasterResponse(byte[] bytes) throws UnsupportedEncodingException, Exception{
        msgheader = new MessageHeader(bytes);
        exchangecode = CConstants.getString(bytes, 6, 2);
    }
}
