package ShareKhanApi.ScripMaster.DataStructs;

import ShareKhanApi.ScripMaster.NCScripMaster;

import java.util.HashMap;
import java.util.Set;

public class ScripMasterHashMap
{
    private HashMap<Integer,String> integerString = new HashMap<>();
    private HashMap<String, Integer> stringInteger = new HashMap<>();

    public ScripMasterHashMap(HashMap<String, NCScripMaster> ncScripMasterHashMap)
    {
        // lambda expressions, fucking more of these please
        ncScripMasterHashMap.forEach((key,value)->
        {
            if("EQ".equalsIgnoreCase(value.segment))
            {
                add(Integer.parseInt(key),value.scripShortName);
                add(value.scripShortName,Integer.parseInt(key));
            }
        });
    }

    private void add(int id, String name)
    {
        integerString.put(id,name);
        stringInteger.put(name,id);
    }

    private void add(String name, int id)
    {
        stringInteger.put(name,id);
        integerString.put(id,name);
    }

    public String get(int id)
    {
        return integerString.get(id);
    }

    public int get(String name)
    {
        return stringInteger.get(name);
    }

    public Set<Integer> getAllIds()
    {
        return integerString.keySet();
    }

    public Set<String> getAllScripNames()
    {
        return stringInteger.keySet();
    }

}
