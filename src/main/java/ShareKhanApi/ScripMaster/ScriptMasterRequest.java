package ShareKhanApi.ScripMaster;

import ShareKhanApi.MessageHeader;
import StaticData.CConstants;

import java.io.UnsupportedEncodingException;

public class ScriptMasterRequest
{
    private MessageHeader msgHeader;
    private String exchangecode;

    public ScriptMasterRequest(MessageHeader msgHeader, String exchangecode) {
        this.msgHeader = msgHeader;
        this.exchangecode = exchangecode;
    }

    public byte[] getAsByteArray() throws UnsupportedEncodingException
    {
        byte[] array = new byte[108];
        System.arraycopy(msgHeader.getByteArray(), 0, array, 0, msgHeader.getByteArray().length);
        CConstants.setString(exchangecode, 2, array, 6);
        return array;
    }
}
