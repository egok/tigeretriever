package ShareKhanApi;

import ShareKhanApi.MarketDepth.MarketDepth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public interface API
{

    /**
     * returns all the scrips
     */
    Set<Integer> getScrips();


    /**
     * Returns all the scripts which matches the ScripFilter
     * @param value ScripFilter
     * @return set of integers, integers are scrip Ids which matches the ScripFilter
     */
    Set<Integer> getScrips(ScripFilter value);

    /**
     * Returns the MarketDepth data for the ScripId
     * @param scripId Integer, the scrip to which the MarketDepth should be requested for
     * @return MarketDepth of the given ScripId
     */
    MarketDepth getMarket(int scripId);

    /**
     * Returns a HashMap of MarketDepth data for the ScripIds
     * @param scripIds ArrayList, the scrips to which the MarketDepth should be requested for
     * @return HashMap of MarketDepth of the given ScripIds
     */
    HashMap<Integer,MarketDepth> getMarket(ArrayList<Integer> scripIds);
}
