package ShareKhanApi.FeedRequest;

import ShareKhanApi.MessageHeader;
import StaticData.CConstants;

import java.io.UnsupportedEncodingException;

public class FeedRequest
{
    MessageHeader header;
    short count;
    String scripList;

    public FeedRequest(short count,String scripList)
    {
        header = new MessageHeader(CConstants.FeedRequestSize, (short) CConstants.TransactionCode.FeedSubscription);
        this.count= count;
        this.scripList = scripList;
    }

    public byte[] getBytes() throws UnsupportedEncodingException {
        byte[] bytes = new byte[CConstants.FeedRequestSize];
        System.arraycopy(header.getByteArray(), 0, bytes, 0, 6);
        CConstants.setBytes(count, bytes, 6);
        CConstants.setString(scripList, 12, bytes, 8);
        return bytes;
    }
}
