package ShareKhanApi;

public class ScripFilter
{
    private String exchange;

    private int priceRangeUpperLimit;
    private int priceRangeLowerLimit;

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public int getPriceRangeUpperLimit() {
        return priceRangeUpperLimit;
    }

    public void setPriceRangeUpperLimit(int priceRangeUpperLimit) {
        this.priceRangeUpperLimit = priceRangeUpperLimit;
    }

    public int getPriceRangeLowerLimit() {
        return priceRangeLowerLimit;
    }

    public void setPriceRangeLowerLimit(int priceRangeLowerLimit) {
        this.priceRangeLowerLimit = priceRangeLowerLimit;
    }
}
