package ShareKhanApi.MarketDepth;

import ShareKhanApi.MessageHeader;
import StaticData.CConstants;

import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MarketDepth
{
    private MessageHeader messageHeader;

    private String exchangeCode;
    private String exchange;
    private String lastTradedTime;
    private String scripCode;

    private int totalBuyQuantity;
    private int totalSellQuantity;

    private int buyPrice1;
    private int buyQuantity1;
    private int buyNumberOfOrder1;

    private int buyPrice2;
    private int buyQuantity2;
    private int buyNumberOfOrder2;

    private int buyPrice3;
    private int buyQuantity3;
    private int buyNumberOfOrder3;

    private int buyPrice4;
    private int buyQuantity4;
    private int buyNumberOfOrder4;

    private int buyPrice5;
    private int buyQuantity5;
    private int buyNumberOfOrder5;

    private int sellPrice1;
    private int sellQuantity1;
    private int sellNumberOfOrder1;

    private int sellPrice2;
    private int sellQuantity2;
    private int sellNumberOfOrder2;

    private int sellPrice3;
    private int sellQuantity3;
    private int sellNumberOfOrder3;

    private int sellPrice4;
    private int sellQuantity4;
    private int sellNumberOfOrder4;

    private int sellPrice5;
    private int sellQuantity5;
    private int sellNumberOfOrder5;

    private String reserved;

    public MarketDepth(byte[] byteStructure)
    {
        messageHeader = new MessageHeader(byteStructure);
        exchangeCode = CConstants.getString(byteStructure,6,5);
        exchange = CConstants.getString(byteStructure,11,10);
        lastTradedTime = CConstants.getString(byteStructure,21,25);
        scripCode = CConstants.getString(byteStructure,46,10);

        totalBuyQuantity = CConstants.getInt32(byteStructure,56);
        totalSellQuantity = CConstants.getInt32(byteStructure,60);

        buyPrice1 = CConstants.getInt32(byteStructure,64);
        buyQuantity1 = CConstants.getInt32(byteStructure,68);
        buyNumberOfOrder1 = CConstants.getInt32(byteStructure,72);

        buyPrice2 = CConstants.getInt32(byteStructure,76);
        buyQuantity2 = CConstants.getInt32(byteStructure,80);
        buyNumberOfOrder2 = CConstants.getInt32(byteStructure,84);

        buyPrice3 = CConstants.getInt32(byteStructure,88);
        buyQuantity3 = CConstants.getInt32(byteStructure,92);
        buyNumberOfOrder3 = CConstants.getInt32(byteStructure,96);

        buyPrice4 = CConstants.getInt32(byteStructure,100);
        buyQuantity4 = CConstants.getInt32(byteStructure,104);
        buyNumberOfOrder4 = CConstants.getInt32(byteStructure,108);

        buyPrice5 = CConstants.getInt32(byteStructure,112);
        buyQuantity5 = CConstants.getInt32(byteStructure,116);
        buyNumberOfOrder5 = CConstants.getInt32(byteStructure,120);

        sellPrice1 = CConstants.getInt32(byteStructure,124);
        sellQuantity1 = CConstants.getInt32(byteStructure,128);
        sellNumberOfOrder1 = CConstants.getInt32(byteStructure,132);

        sellPrice2 = CConstants.getInt32(byteStructure,136);
        sellQuantity2 = CConstants.getInt32(byteStructure,140);
        sellNumberOfOrder2 = CConstants.getInt32(byteStructure,144);

        sellPrice3 = CConstants.getInt32(byteStructure,148);
        sellQuantity3 = CConstants.getInt32(byteStructure,152);
        sellNumberOfOrder3 = CConstants.getInt32(byteStructure,156);

        sellPrice4 = CConstants.getInt32(byteStructure,160);
        sellQuantity4 = CConstants.getInt32(byteStructure,164);
        sellNumberOfOrder4 = CConstants.getInt32(byteStructure,168);

        sellPrice5 = CConstants.getInt32(byteStructure,172);
        sellQuantity5 = CConstants.getInt32(byteStructure,176);
        sellNumberOfOrder5 = CConstants.getInt32(byteStructure,180);

        reserved = CConstants.getString(byteStructure,184,100);

    }

    public MessageHeader getMessageHeader() {
        return messageHeader;
    }
    /*
    * **************************************
    * */
    public String getExchangeCode() {
        return exchangeCode;
    }

    public String getExchange() {
        return exchange;
    }

    public String getLastTradedTime() {
        return lastTradedTime;
    }

    public String getScripCode() {
        return scripCode;
    }
    /*
     * **************************************
     * */
    public int getTotalBuyQuantity() {
        return totalBuyQuantity;
    }

    public int getTotalSellQuantity() {
        return totalSellQuantity;
    }

    /*
     * **************************************
     * */
    public float getBuyPrice1() {
        return buyPrice1/(float)100.00;
    }

    public int getBuyQuantity1() {
        return buyQuantity1;
    }

    public int getBuyNumberOfOrder1() {
        return buyNumberOfOrder1;
    }

    /*
     * **************************************
     * */
    public float getBuyPrice2() {
        return buyPrice2/(float)100.00;
    }

    public int getBuyQuantity2() {
        return buyQuantity2;
    }

    public int getBuyNumberOfOrder2() {
        return buyNumberOfOrder2;
    }

    /*
     * **************************************
     * */
    public float getBuyPrice3() {
        return buyPrice3/(float)100.00;
    }

    public int getBuyQuantity3() {
        return buyQuantity3;
    }

    public int getBuyNumberOfOrder3() {
        return buyNumberOfOrder3;
    }

    /*
     * **************************************
     * */
    public float getBuyPrice4() {
        return buyPrice4/(float)100.00;
    }

    public int getBuyQuantity4() {
        return buyQuantity4;
    }

    public int getBuyNumberOfOrder4() {
        return buyNumberOfOrder4;
    }

    /*
     * **************************************
     * */
    public float getBuyPrice5() {
        return buyPrice5/(float)100.00;
    }

    public int getBuyQuantity5() {
        return buyQuantity5;
    }

    public int getBuyNumberOfOrder5() {
        return buyNumberOfOrder5;
    }

    /*
     * **************************************
     * */
    public float getSellPrice1() {
        return sellPrice1/(float)100.00;
    }

    public int getSellQuantity1() {
        return sellQuantity1;
    }

    public int getSellNumberOfOrder1() {
        return sellNumberOfOrder1;
    }

    /*
     * **************************************
     * */
    public float getSellPrice2() {
        return sellPrice2/(float)100.00;
    }

    public int getSellQuantity2() {
        return sellQuantity2;
    }

    public int getSellNumberOfOrder2() {
        return sellNumberOfOrder2;
    }

    /*
     * **************************************
     * */
    public float getSellPrice3() {
        return sellPrice3/(float)100.00;
    }

    public int getSellQuantity3() {
        return sellQuantity3;
    }

    public int getSellNumberOfOrder3() {
        return sellNumberOfOrder3;
    }

    /*
     * **************************************
     * */
    public float getSellPrice4() {
        return sellPrice4/(float)100.00;
    }

    public int getSellQuantity4() {
        return sellQuantity4;
    }

    public int getSellNumberOfOrder4() {
        return sellNumberOfOrder4;
    }

    /*
     * **************************************
     * */
    public float getSellPrice5() {
        return sellPrice5/(float)100.00;
    }

    public int getSellQuantity5() {
        return sellQuantity5;
    }

    public int getSellNumberOfOrder5() {
        return sellNumberOfOrder5;
    }

    /*
     * **************************************
     * */
    public String getReserved() {
        return reserved;
    }

    @Override
    public String toString() {
        return messageHeader.toString() + "|" +
                " Exchange = " + exchange + "|" +
                " Exchange Code = " + exchangeCode + "|" +
                " Last Traded Time = " + lastTradedTime + "|" +
                " Scrip Code = " + scripCode + "|" +

                " Total Buy Quantity = " + totalBuyQuantity + "|" +
                " Total Sell Quantity  = " + totalSellQuantity + "|" +

                " Buy Price1 = " + buyPrice1/(float)100.00 + "|" +
                " Buy Quantity1  = " + buyQuantity1 + "|" +
                " Buy NumberOfOrder1 = " + buyNumberOfOrder1 + "|" +

                " Buy Price2 = " + buyPrice2/(float)100.00 + "|" +
                " Buy Quantity2 = " + buyQuantity2 + "|" +
                " Buy NumberOfOrder2 = " + buyNumberOfOrder2 + "|" +

                " Buy Price3 = " + buyPrice3/(float)100.00 + "|" +
                " Buy Quantity3 = " + buyQuantity3 + "|" +
                " Buy NumberOfOrder3 = " + buyNumberOfOrder3 + "|" +

                " Buy Price4 = " + buyPrice4/(float)100.00 + "|" +
                " Buy Quantity4 = " + buyQuantity4 + "|" +
                " Buy NumberOfOrder4 = " + buyNumberOfOrder4 + "|" +

                " Buy Price5 = " + buyPrice5/(float)100.00 + "|" +
                " Buy Quantity5 = " + buyQuantity5 + "|" +
                " Buy NumberOfOrder5 = " + buyNumberOfOrder5 + "|" +

                " Sell Price1 = " + sellPrice1/(float)100.00 + "|" +
                " Sell Quantity1 = " + sellQuantity1 + "|" +
                " Sell NumberOfOrder1 = " + sellNumberOfOrder1 + "|" +

                " Sell Price2 = " + sellPrice2/(float)100.00 + "|" +
                " Sell Quantity2 = " + sellQuantity2 + "|" +
                " Sell NumberOfOrder2 = " + sellNumberOfOrder2 + "|" +

                " Sell Price3 = " + sellPrice3/(float)100.00 + "|" +
                " Sell Quantity3 = " + sellQuantity3 + "|" +
                " Sell NumberOfOrder3 = " + sellNumberOfOrder3 + "|" +

                " Sell Price4 = " + sellPrice4/(float)100.00 + "|" +
                " Sell Quantity4 = " + sellQuantity4 + "|" +
                " Sell NumberOfOrder4 = " + sellNumberOfOrder4 + "|" +

                " Sell Price5 = " + sellPrice5/(float)100.00 + "|" +
                " Sell Quantity5 = " + sellQuantity5 + "|" +
                " Sell NumberOfOrder5 = " + sellNumberOfOrder5 + "|" +
                " Reserved = " + reserved;
    }
}
