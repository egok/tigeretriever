package StaticData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExchangeCodes
{
    private static ExchangeCodes myExchangeCodesClass;
    private ArrayList<String> myExchangeCodes = new ArrayList<String>();
    public static HashMap<String, ExchangeCode> myDetailedExchangeCodes;

    private ExchangeCodes()
    {
        ExchangeCode myNCExchangeCode = new ExchangeCode("NC", "NSE Cash", "National Stock Exchange Equities Segment ");
        myExchangeCodes.add(myNCExchangeCode.getExchangeCode());
        myDetailedExchangeCodes.put(myNCExchangeCode.getExchangeCode(),myNCExchangeCode);

        ExchangeCode myBCExchangeCode = new ExchangeCode("BC", "BSE Cash", "Bombay Stock Exchange Equities Segment");
        myExchangeCodes.add(myBCExchangeCode.getExchangeCode());
        myDetailedExchangeCodes.put(myBCExchangeCode.getExchangeCode(), myBCExchangeCode);

        ExchangeCode myNFExchangeCode = new ExchangeCode("NF", "NSE Futures and Options","National Stock Exchange Derivative Segment" );
        myExchangeCodes.add(myNFExchangeCode.getExchangeCode());
        myDetailedExchangeCodes.put(myNFExchangeCode.getExchangeCode(), myNFExchangeCode);

        ExchangeCode myMXExchangeCode = new ExchangeCode("MX", "MCX", "Multi Commodity Exchange" );
        myExchangeCodes.add(myMXExchangeCode.getExchangeCode());
        myDetailedExchangeCodes.put(myMXExchangeCode.getExchangeCode(), myMXExchangeCode);

        ExchangeCode myNXExchangeCode = new ExchangeCode("NX", "NCDEX", "National Commodity & Derivative Exchange");
        myExchangeCodes.add(myNXExchangeCode.getExchangeCode());
        myDetailedExchangeCodes.put(myNXExchangeCode.getExchangeCode(),myNXExchangeCode);

        ExchangeCode myRNExchangeCode = new ExchangeCode("RN", "NSE", "CURRENCY National Stock Exchange");
        myExchangeCodes.add(myRNExchangeCode.getExchangeCode());
        myDetailedExchangeCodes.put(myRNExchangeCode.getExchangeCode(), myRNExchangeCode);

        ExchangeCode myRMExchangeCode = new ExchangeCode("RM", "MCX", "CURRENCY Multi Commodity Exchange");
        myExchangeCodes.add(myRMExchangeCode.getExchangeCode());
        myDetailedExchangeCodes.put( myRMExchangeCode.getExchangeCode(), myRMExchangeCode);
    }

    public static ExchangeCodes getInstance()
    {
        if (myExchangeCodesClass == null)
        {
            myExchangeCodesClass = new ExchangeCodes();
        }
        return myExchangeCodesClass;
    }

    public ArrayList<String> getExchangeCodes()
    {
        return myExchangeCodes;
    }

    public HashMap getDetailedExchangeCodes()
    {
        return myDetailedExchangeCodes;
    }
}
