package StaticData;

public class ExchangeCode
{
    private String exchangeCode;
    private String exchange;
    private String description;

    public ExchangeCode(String exchangeCode,
            String exchange,
            String description)
    {
        this.exchange = exchange;
        this.exchangeCode = exchangeCode;
        this.description = description;
    }

    public String getExchangeCode()
    {
        return exchangeCode;
    }

    public String getExchange() {
        return exchange;
    }

    public String getDescription() {
        return description;
    }
}
