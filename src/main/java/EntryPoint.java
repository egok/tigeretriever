import ShareKhanApi.APIImpl;
import ShareKhanApi.ScripFilter;
import ShareKhanApi.ScripMaster.ScripMasterCore;

import java.util.Set;

public class EntryPoint
{
    public static void main(String args[])
    {
        APIImpl apiImpl = new APIImpl();
        apiImpl.initilizeShareKhanAPI();
        apiImpl.initilizeScripObjects();

        ScripFilter scripFilter = new ScripFilter();
        scripFilter.setPriceRangeLowerLimit(0);
        scripFilter.setPriceRangeUpperLimit(100);

        Set<Integer> filteredScripIds = apiImpl.getScrips(scripFilter);

        filteredScripIds.forEach(
                id->
                {
                    System.out.println("Scrip Name: " + apiImpl.scripMasterHashMap.get(id) +
                            "| Scrip ID: " + id);
                });

    }
}
